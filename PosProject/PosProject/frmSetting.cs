﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevComponents.DotNetBar;
namespace PosProject
{
    public partial class frmSetting : Office2007Form
    {
        public frmSetting()
        {
            InitializeComponent();
        }
        PosDBEntities db;
        PSetting PSetting;
        private void FrmSetting_Load(object sender, EventArgs e)
        {
            db = new PosDBEntities();
            if (!db.PSettings.Any())
            {
                PSetting = new PSetting()
                {
                    DefaultBank = "ملت",
                    Protocol = "Lan",
                };
                db.PSettings.Add(PSetting);
                db.SaveChanges();
            }

            PSetting = db.PSettings.Where(c => c.Id == 1).FirstOrDefault();

            int index = cmbBank.FindString(PSetting.DefaultBank);
            cmbBank.SelectedIndex = index;

            int index2 = cmbProtocol.FindString(PSetting.Protocol);
            cmbProtocol.SelectedIndex = index2;


            foreach (string port in System.IO.Ports.SerialPort.GetPortNames())
                cmbPort.Items.Add(port);

            if (!string.IsNullOrEmpty(PSetting.Port))
            {
                int index3 = cmbPort.FindString(PSetting.Port);
                cmbPort.SelectedIndex = index3;
            }

            txtPortNumber.Text = PSetting.PortNumber;
            txtIP.Text = PSetting.IP;
            txtAcceptorId.Text = PSetting.AcceptorId;
            txtTerminalId.Text = PSetting.TerminalId;
            txtPosId.Text = PSetting.PosId;
            txtTimeOut.Text = PSetting.TimeOut.ToString();
            txtTextOnBill.Text = PSetting.TextOnBill;
        }

        private void ButtonX1_Click(object sender, EventArgs e)
        {
            PSetting.DefaultBank = cmbBank.Text;
            PSetting.Protocol = cmbProtocol.Text;
            PSetting.Port = cmbPort.Text;
            PSetting.PortNumber = txtPortNumber.Text;
            PSetting.IP = txtIP.Text;
            PSetting.AcceptorId = txtAcceptorId.Text;
            PSetting.TerminalId = txtTerminalId.Text;
            PSetting.PosId = txtPosId.Text;
            PSetting.TimeOut = int.Parse(txtTimeOut.Text);
            PSetting.TextOnBill = txtTextOnBill.Text;

            db.SaveChanges();
            this.Close();
        }
    }
}
