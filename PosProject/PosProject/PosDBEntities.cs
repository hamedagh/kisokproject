namespace PosProject
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Data.Entity;
    using System.Linq;

    public class PosDBEntities : DbContext
    {
        // Your context has been configured to use a 'PosDBEntities' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'PosProject.PosDBEntities' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'PosDBEntities' 
        // connection string in the application configuration file.
        public PosDBEntities()
            : base("name=PosDBEntities")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

         public virtual DbSet<PSetting> PSettings { get; set; }
    }

    public class PSetting
    {
        public int Id { get; set; }
        [MaxLength(50)]
        public string DefaultBank { get; set; }
        [MaxLength(50)]
        public string Protocol { get; set; }
        [MaxLength(50)]
        public string Port { get; set; }
        [MaxLength(50)]
        public string PortNumber { get; set; }
        [MaxLength(50)]
        public string IP { get; set; }
        [MaxLength(50)]
        public string AcceptorId { get; set; }
        [MaxLength(50)]
        public string TerminalId { get; set; }
        [MaxLength(50)]
        public string PosId { get; set; }
        public int TimeOut { get; set; }
        [MaxLength(50)]
        public string TextOnBill { get; set; }
    }
}