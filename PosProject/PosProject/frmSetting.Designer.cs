﻿namespace PosProject
{
    partial class frmSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelEx1 = new DevComponents.DotNetBar.PanelEx();
            this.cmbBank = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem4 = new DevComponents.Editors.ComboItem();
            this.comboItem5 = new DevComponents.Editors.ComboItem();
            this.comboItem6 = new DevComponents.Editors.ComboItem();
            this.comboItem7 = new DevComponents.Editors.ComboItem();
            this.comboItem8 = new DevComponents.Editors.ComboItem();
            this.comboItem9 = new DevComponents.Editors.ComboItem();
            this.comboItem10 = new DevComponents.Editors.ComboItem();
            this.labelX11 = new DevComponents.DotNetBar.LabelX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.labelX2 = new DevComponents.DotNetBar.LabelX();
            this.labelX1 = new DevComponents.DotNetBar.LabelX();
            this.labelX8 = new DevComponents.DotNetBar.LabelX();
            this.labelX9 = new DevComponents.DotNetBar.LabelX();
            this.labelX7 = new DevComponents.DotNetBar.LabelX();
            this.labelX6 = new DevComponents.DotNetBar.LabelX();
            this.labelX5 = new DevComponents.DotNetBar.LabelX();
            this.labelX4 = new DevComponents.DotNetBar.LabelX();
            this.labelX10 = new DevComponents.DotNetBar.LabelX();
            this.labelX3 = new DevComponents.DotNetBar.LabelX();
            this.txtTimeOut = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTextOnBill = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtPosId = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtTerminalId = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtAcceptorId = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtPortNumber = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.txtIP = new DevComponents.DotNetBar.Controls.TextBoxX();
            this.cmbPort = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.cmbProtocol = new DevComponents.DotNetBar.Controls.ComboBoxEx();
            this.comboItem1 = new DevComponents.Editors.ComboItem();
            this.comboItem2 = new DevComponents.Editors.ComboItem();
            this.comboItem3 = new DevComponents.Editors.ComboItem();
            this.panelEx1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelEx1
            // 
            this.panelEx1.CanvasColor = System.Drawing.SystemColors.Control;
            this.panelEx1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.panelEx1.Controls.Add(this.cmbBank);
            this.panelEx1.Controls.Add(this.labelX11);
            this.panelEx1.Controls.Add(this.buttonX1);
            this.panelEx1.Controls.Add(this.labelX2);
            this.panelEx1.Controls.Add(this.labelX1);
            this.panelEx1.Controls.Add(this.labelX8);
            this.panelEx1.Controls.Add(this.labelX9);
            this.panelEx1.Controls.Add(this.labelX7);
            this.panelEx1.Controls.Add(this.labelX6);
            this.panelEx1.Controls.Add(this.labelX5);
            this.panelEx1.Controls.Add(this.labelX4);
            this.panelEx1.Controls.Add(this.labelX10);
            this.panelEx1.Controls.Add(this.labelX3);
            this.panelEx1.Controls.Add(this.txtTimeOut);
            this.panelEx1.Controls.Add(this.txtTextOnBill);
            this.panelEx1.Controls.Add(this.txtPosId);
            this.panelEx1.Controls.Add(this.txtTerminalId);
            this.panelEx1.Controls.Add(this.txtAcceptorId);
            this.panelEx1.Controls.Add(this.txtPortNumber);
            this.panelEx1.Controls.Add(this.txtIP);
            this.panelEx1.Controls.Add(this.cmbPort);
            this.panelEx1.Controls.Add(this.cmbProtocol);
            this.panelEx1.DisabledBackColor = System.Drawing.Color.Empty;
            this.panelEx1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEx1.Location = new System.Drawing.Point(0, 0);
            this.panelEx1.Name = "panelEx1";
            this.panelEx1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panelEx1.Size = new System.Drawing.Size(351, 350);
            this.panelEx1.Style.Alignment = System.Drawing.StringAlignment.Center;
            this.panelEx1.Style.BackColor1.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.panelEx1.Style.Border = DevComponents.DotNetBar.eBorderType.SingleLine;
            this.panelEx1.Style.BorderColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.panelEx1.Style.ForeColor.ColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.panelEx1.Style.GradientAngle = 90;
            this.panelEx1.TabIndex = 2;
            // 
            // cmbBank
            // 
            this.cmbBank.DisplayMember = "Text";
            this.cmbBank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBank.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.cmbBank.ForeColor = System.Drawing.Color.Black;
            this.cmbBank.FormattingEnabled = true;
            this.cmbBank.ItemHeight = 13;
            this.cmbBank.Items.AddRange(new object[] {
            this.comboItem4,
            this.comboItem5,
            this.comboItem6,
            this.comboItem7,
            this.comboItem8,
            this.comboItem9,
            this.comboItem10});
            this.cmbBank.Location = new System.Drawing.Point(8, 14);
            this.cmbBank.Name = "cmbBank";
            this.cmbBank.Size = new System.Drawing.Size(217, 21);
            this.cmbBank.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbBank.TabIndex = 40;
            // 
            // comboItem4
            // 
            this.comboItem4.Text = "سامان کیش";
            // 
            // comboItem5
            // 
            this.comboItem5.Text = "تجارت الکترونیک پارسیان";
            // 
            // comboItem6
            // 
            this.comboItem6.Text = "ملی";
            // 
            // comboItem7
            // 
            this.comboItem7.Text = "ایران کیش";
            // 
            // comboItem8
            // 
            this.comboItem8.Text = "آسان پرداخت";
            // 
            // comboItem9
            // 
            this.comboItem9.Text = "ملت";
            // 
            // comboItem10
            // 
            this.comboItem10.Text = "به پرداخت";
            // 
            // labelX11
            // 
            // 
            // 
            // 
            this.labelX11.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX11.Location = new System.Drawing.Point(232, 14);
            this.labelX11.Name = "labelX11";
            this.labelX11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX11.Size = new System.Drawing.Size(81, 21);
            this.labelX11.TabIndex = 39;
            this.labelX11.Text = "بانک/شرکت:";
            this.labelX11.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.FocusCuesEnabled = false;
            this.buttonX1.Location = new System.Drawing.Point(127, 313);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(98, 25);
            this.buttonX1.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.buttonX1.TabIndex = 37;
            this.buttonX1.Text = "ذخیره";
            this.buttonX1.Click += new System.EventHandler(this.ButtonX1_Click);
            // 
            // labelX2
            // 
            // 
            // 
            // 
            this.labelX2.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX2.Location = new System.Drawing.Point(231, 42);
            this.labelX2.Name = "labelX2";
            this.labelX2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX2.Size = new System.Drawing.Size(81, 21);
            this.labelX2.TabIndex = 36;
            this.labelX2.Text = "پروتکل ارتباطی:";
            this.labelX2.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX1
            // 
            // 
            // 
            // 
            this.labelX1.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX1.Location = new System.Drawing.Point(8, 251);
            this.labelX1.Name = "labelX1";
            this.labelX1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX1.Size = new System.Drawing.Size(25, 25);
            this.labelX1.TabIndex = 34;
            this.labelX1.Text = "ثانیه";
            // 
            // labelX8
            // 
            // 
            // 
            // 
            this.labelX8.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX8.Location = new System.Drawing.Point(231, 251);
            this.labelX8.Name = "labelX8";
            this.labelX8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX8.Size = new System.Drawing.Size(81, 25);
            this.labelX8.TabIndex = 33;
            this.labelX8.Text = "مدت زمان انتظار:";
            this.labelX8.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX9
            // 
            // 
            // 
            // 
            this.labelX9.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelX9.Location = new System.Drawing.Point(231, 282);
            this.labelX9.Name = "labelX9";
            this.labelX9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX9.Size = new System.Drawing.Size(112, 25);
            this.labelX9.TabIndex = 32;
            this.labelX9.Text = "متن روی رسید مشتری:";
            this.labelX9.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX7
            // 
            // 
            // 
            // 
            this.labelX7.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX7.Location = new System.Drawing.Point(231, 220);
            this.labelX7.Name = "labelX7";
            this.labelX7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX7.Size = new System.Drawing.Size(81, 25);
            this.labelX7.TabIndex = 31;
            this.labelX7.Text = "شناسه دستگاه:";
            this.labelX7.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX6
            // 
            // 
            // 
            // 
            this.labelX6.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX6.Location = new System.Drawing.Point(231, 189);
            this.labelX6.Name = "labelX6";
            this.labelX6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX6.Size = new System.Drawing.Size(81, 25);
            this.labelX6.TabIndex = 30;
            this.labelX6.Text = "شماره پایانه:";
            this.labelX6.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX5
            // 
            // 
            // 
            // 
            this.labelX5.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX5.Location = new System.Drawing.Point(231, 158);
            this.labelX5.Name = "labelX5";
            this.labelX5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX5.Size = new System.Drawing.Size(81, 25);
            this.labelX5.TabIndex = 35;
            this.labelX5.Text = "شماره پذیرنده:";
            this.labelX5.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX4
            // 
            // 
            // 
            // 
            this.labelX4.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX4.Location = new System.Drawing.Point(231, 69);
            this.labelX4.Name = "labelX4";
            this.labelX4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX4.Size = new System.Drawing.Size(81, 21);
            this.labelX4.TabIndex = 29;
            this.labelX4.Text = "پورت:";
            this.labelX4.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX10
            // 
            // 
            // 
            // 
            this.labelX10.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX10.Location = new System.Drawing.Point(231, 96);
            this.labelX10.Name = "labelX10";
            this.labelX10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX10.Size = new System.Drawing.Size(81, 25);
            this.labelX10.TabIndex = 28;
            this.labelX10.Text = "شماره پورت:";
            this.labelX10.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // labelX3
            // 
            // 
            // 
            // 
            this.labelX3.BackgroundStyle.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.labelX3.Location = new System.Drawing.Point(231, 127);
            this.labelX3.Name = "labelX3";
            this.labelX3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelX3.Size = new System.Drawing.Size(81, 25);
            this.labelX3.TabIndex = 27;
            this.labelX3.Text = "آدرس IP:";
            this.labelX3.TextAlignment = System.Drawing.StringAlignment.Far;
            // 
            // txtTimeOut
            // 
            this.txtTimeOut.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTimeOut.Border.Class = "TextBoxBorder";
            this.txtTimeOut.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTimeOut.DisabledBackColor = System.Drawing.Color.White;
            this.txtTimeOut.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtTimeOut.ForeColor = System.Drawing.Color.Black;
            this.txtTimeOut.Location = new System.Drawing.Point(39, 251);
            this.txtTimeOut.Multiline = true;
            this.txtTimeOut.Name = "txtTimeOut";
            this.txtTimeOut.PreventEnterBeep = true;
            this.txtTimeOut.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTimeOut.Size = new System.Drawing.Size(186, 25);
            this.txtTimeOut.TabIndex = 26;
            // 
            // txtTextOnBill
            // 
            this.txtTextOnBill.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTextOnBill.Border.Class = "TextBoxBorder";
            this.txtTextOnBill.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTextOnBill.DisabledBackColor = System.Drawing.Color.White;
            this.txtTextOnBill.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtTextOnBill.ForeColor = System.Drawing.Color.Black;
            this.txtTextOnBill.Location = new System.Drawing.Point(8, 282);
            this.txtTextOnBill.Multiline = true;
            this.txtTextOnBill.Name = "txtTextOnBill";
            this.txtTextOnBill.PreventEnterBeep = true;
            this.txtTextOnBill.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTextOnBill.Size = new System.Drawing.Size(217, 25);
            this.txtTextOnBill.TabIndex = 25;
            // 
            // txtPosId
            // 
            this.txtPosId.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPosId.Border.Class = "TextBoxBorder";
            this.txtPosId.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPosId.DisabledBackColor = System.Drawing.Color.White;
            this.txtPosId.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtPosId.ForeColor = System.Drawing.Color.Black;
            this.txtPosId.Location = new System.Drawing.Point(8, 220);
            this.txtPosId.Multiline = true;
            this.txtPosId.Name = "txtPosId";
            this.txtPosId.PreventEnterBeep = true;
            this.txtPosId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPosId.Size = new System.Drawing.Size(217, 25);
            this.txtPosId.TabIndex = 24;
            // 
            // txtTerminalId
            // 
            this.txtTerminalId.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtTerminalId.Border.Class = "TextBoxBorder";
            this.txtTerminalId.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtTerminalId.DisabledBackColor = System.Drawing.Color.White;
            this.txtTerminalId.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtTerminalId.ForeColor = System.Drawing.Color.Black;
            this.txtTerminalId.Location = new System.Drawing.Point(8, 189);
            this.txtTerminalId.Multiline = true;
            this.txtTerminalId.Name = "txtTerminalId";
            this.txtTerminalId.PreventEnterBeep = true;
            this.txtTerminalId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTerminalId.Size = new System.Drawing.Size(217, 25);
            this.txtTerminalId.TabIndex = 23;
            // 
            // txtAcceptorId
            // 
            this.txtAcceptorId.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtAcceptorId.Border.Class = "TextBoxBorder";
            this.txtAcceptorId.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtAcceptorId.DisabledBackColor = System.Drawing.Color.White;
            this.txtAcceptorId.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtAcceptorId.ForeColor = System.Drawing.Color.Black;
            this.txtAcceptorId.Location = new System.Drawing.Point(8, 158);
            this.txtAcceptorId.Multiline = true;
            this.txtAcceptorId.Name = "txtAcceptorId";
            this.txtAcceptorId.PreventEnterBeep = true;
            this.txtAcceptorId.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAcceptorId.Size = new System.Drawing.Size(217, 25);
            this.txtAcceptorId.TabIndex = 22;
            // 
            // txtPortNumber
            // 
            this.txtPortNumber.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtPortNumber.Border.Class = "TextBoxBorder";
            this.txtPortNumber.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtPortNumber.DisabledBackColor = System.Drawing.Color.White;
            this.txtPortNumber.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtPortNumber.ForeColor = System.Drawing.Color.Black;
            this.txtPortNumber.Location = new System.Drawing.Point(8, 96);
            this.txtPortNumber.Multiline = true;
            this.txtPortNumber.Name = "txtPortNumber";
            this.txtPortNumber.PreventEnterBeep = true;
            this.txtPortNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtPortNumber.Size = new System.Drawing.Size(217, 25);
            this.txtPortNumber.TabIndex = 21;
            // 
            // txtIP
            // 
            this.txtIP.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.txtIP.Border.Class = "TextBoxBorder";
            this.txtIP.Border.CornerType = DevComponents.DotNetBar.eCornerType.Square;
            this.txtIP.DisabledBackColor = System.Drawing.Color.White;
            this.txtIP.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.txtIP.ForeColor = System.Drawing.Color.Black;
            this.txtIP.Location = new System.Drawing.Point(8, 127);
            this.txtIP.Multiline = true;
            this.txtIP.Name = "txtIP";
            this.txtIP.PreventEnterBeep = true;
            this.txtIP.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtIP.Size = new System.Drawing.Size(217, 25);
            this.txtIP.TabIndex = 20;
            // 
            // cmbPort
            // 
            this.cmbPort.DisplayMember = "Text";
            this.cmbPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPort.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.cmbPort.ForeColor = System.Drawing.Color.Black;
            this.cmbPort.FormattingEnabled = true;
            this.cmbPort.ItemHeight = 13;
            this.cmbPort.Location = new System.Drawing.Point(8, 69);
            this.cmbPort.Name = "cmbPort";
            this.cmbPort.Size = new System.Drawing.Size(217, 21);
            this.cmbPort.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbPort.TabIndex = 19;
            // 
            // cmbProtocol
            // 
            this.cmbProtocol.DisplayMember = "Text";
            this.cmbProtocol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbProtocol.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.cmbProtocol.ForeColor = System.Drawing.Color.Black;
            this.cmbProtocol.FormattingEnabled = true;
            this.cmbProtocol.ItemHeight = 13;
            this.cmbProtocol.Items.AddRange(new object[] {
            this.comboItem1,
            this.comboItem2,
            this.comboItem3});
            this.cmbProtocol.Location = new System.Drawing.Point(8, 42);
            this.cmbProtocol.Name = "cmbProtocol";
            this.cmbProtocol.Size = new System.Drawing.Size(217, 21);
            this.cmbProtocol.Style = DevComponents.DotNetBar.eDotNetBarStyle.StyleManagerControlled;
            this.cmbProtocol.TabIndex = 18;
            // 
            // comboItem1
            // 
            this.comboItem1.Text = "Lan";
            // 
            // comboItem2
            // 
            this.comboItem2.Text = "USB";
            // 
            // comboItem3
            // 
            this.comboItem3.Text = "COM";
            // 
            // frmSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(351, 350);
            this.Controls.Add(this.panelEx1);
            this.DoubleBuffered = true;
            this.EnableGlass = false;
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "تنظیمات کارتخوان";
            this.Load += new System.EventHandler(this.FrmSetting_Load);
            this.panelEx1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevComponents.DotNetBar.PanelEx panelEx1;
        private DevComponents.DotNetBar.LabelX labelX11;
        private DevComponents.DotNetBar.ButtonX buttonX1;
        private DevComponents.DotNetBar.LabelX labelX2;
        private DevComponents.DotNetBar.LabelX labelX1;
        private DevComponents.DotNetBar.LabelX labelX8;
        private DevComponents.DotNetBar.LabelX labelX9;
        private DevComponents.DotNetBar.LabelX labelX7;
        private DevComponents.DotNetBar.LabelX labelX6;
        private DevComponents.DotNetBar.LabelX labelX5;
        private DevComponents.DotNetBar.LabelX labelX4;
        private DevComponents.DotNetBar.LabelX labelX10;
        private DevComponents.DotNetBar.LabelX labelX3;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTimeOut;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTextOnBill;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPosId;
        private DevComponents.DotNetBar.Controls.TextBoxX txtTerminalId;
        private DevComponents.DotNetBar.Controls.TextBoxX txtAcceptorId;
        private DevComponents.DotNetBar.Controls.TextBoxX txtPortNumber;
        private DevComponents.DotNetBar.Controls.TextBoxX txtIP;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbPort;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbProtocol;
        private DevComponents.Editors.ComboItem comboItem1;
        private DevComponents.Editors.ComboItem comboItem2;
        private DevComponents.Editors.ComboItem comboItem3;
        private DevComponents.DotNetBar.Controls.ComboBoxEx cmbBank;
        private DevComponents.Editors.ComboItem comboItem4;
        private DevComponents.Editors.ComboItem comboItem5;
        private DevComponents.Editors.ComboItem comboItem6;
        private DevComponents.Editors.ComboItem comboItem7;
        private DevComponents.Editors.ComboItem comboItem8;
        private DevComponents.Editors.ComboItem comboItem9;
        private DevComponents.Editors.ComboItem comboItem10;
    }
}