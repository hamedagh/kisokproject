import 'package:flutter/material.dart';

class SettingDropDownItems extends StatefulWidget {
  const SettingDropDownItems({super.key, required this.onChanged});
  final Function(int value) onChanged;

  @override
  State<SettingDropDownItems> createState() => _SettingDropDownItemsState();
}

class _SettingDropDownItemsState extends State<SettingDropDownItems> {
  final items = [
    "تجارت الکترونیک پارسیان",
    " سامان کیش",
    "ملی",
    "ایران کیش",
    "آسان پرداخت",
    "ملت",
    "به پرداخت"
  ];
  String selectedValue = "تجارت الکترونیک پارسیان";
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 300,
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.black, width: 1),
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: DropdownButtonHideUnderline(
            child: DropdownButton<String>(
              value: selectedValue,
              style: TextStyle(color: Colors.amber),
              isExpanded: true,
              items: items.map(buildMenuItem).toList(),
              onChanged: ((value) {
                selectedValue = value!;
                widget.onChanged.call(items.indexOf(value));
                setState(() {});
              }),
            ),
          ),
        ));
  }

  DropdownMenuItem<String> buildMenuItem(String item) => DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: TextStyle(fontSize: 16, color: Colors.black),
        ),
      );
}
