class RequestModel {
  RequestModel(
      {this.bankName,
      this.protocol,
      this.portNumber,
      this.ipAddress,
      this.acceptorNumber,
      this.payanehNumber,
      this.deviceNumber,
      this.comment,
      this.timeDelay});
  int? bankName;
  String? protocol;
  int? portNumber;
  String? ipAddress;
  String? acceptorNumber;
  String? payanehNumber;
  String? deviceNumber;
  int? timeDelay;
  String? comment;
  int? amount;

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['bankName'] = bankName;
    data['protocol'] = protocol;
    data['portNumber'] = portNumber;
    data['ipAddress'] = ipAddress;
    data['acceptorNumber'] = acceptorNumber;
    data['payanehNumber'] = payanehNumber;
    data['deviceNumber'] = deviceNumber;
    data['timeDelay'] = timeDelay;
    data['comment'] = comment;
    data['amount'] = amount;

    return data;
  }
}
