import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kiosk/request_model.dart';
import 'package:process_run/shell.dart';

import 'setting_drop_down_items.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int bankIndex = 0;
  TextEditingController portController = TextEditingController();
  TextEditingController ipController = TextEditingController();
  TextEditingController acceptorNumberController = TextEditingController();
  TextEditingController payneNumberController = TextEditingController();
  TextEditingController deviceNumberController = TextEditingController();
  TextEditingController delayTimeController = TextEditingController();
  TextEditingController customerTextController = TextEditingController();
  TextEditingController amountTextController = TextEditingController();

  void _incrementCounter() async {
    var shell = Shell();

    await shell.run('''
if not exist "env" (mkdir "env")
  cd env
    pip install pythonnet
    python env/main.py  ''');
    var input =
        await File("${Directory.current.path}/data.json").readAsString();
    var map = jsonDecode(input);
    responseDialog(map['responseMsg']);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: (() {
          _incrementCounter();
        }),
      ),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 100),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () => showSettingDialog(),
              child: Container(
                padding: const EdgeInsets.all(8),
                decoration: const BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: const Text("تنظیمات دستگاه پوز"),
              ),
            ),
            InkWell(
              onTap: () => amountDialog(),
              child: Container(
                padding: const EdgeInsets.all(8),
                decoration: const BoxDecoration(
                  color: Colors.amber,
                  borderRadius: BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
                child: const Text("ارسال مبلغ به پوز"),
              ),
            ),
          ],
        ),
      ),
    );
  }

  responseDialog(final String response) {
    return showDialog(
      context: context,
      builder: ((context) {
        return StatefulBuilder(builder:
            (BuildContext context, void Function(void Function()) setState) {
          return AlertDialog(
            content: Directionality(
              textDirection: TextDirection.rtl,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [Text(response)],
              ),
            ),
          );
        });
      }),
    );
  }

  amountDialog() {
    return showDialog(
      context: context,
      builder: ((context) {
        return StatefulBuilder(builder:
            (BuildContext context, void Function(void Function()) setState) {
          return AlertDialog(
            content: Directionality(
              textDirection: TextDirection.rtl,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  textFieldWidget(
                      label: "مبلغ را وارد کنید",
                      controller: amountTextController),
                  const SizedBox(
                    height: 14,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        iconSize: 40,
                        onPressed: (() {
                          RequestModel requestModel = RequestModel();
                          requestModel.amount =
                              int.tryParse(amountTextController.text);
                          requestModel.bankName = bankIndex;
                          requestModel.ipAddress = ipController.text;
                          requestModel.portNumber =
                              int.tryParse(portController.text);
                          requestModel.protocol = "LAN";
                          requestModel.payanehNumber =
                              payneNumberController.text;
                          requestModel.acceptorNumber =
                              acceptorNumberController.text;
                          requestModel.comment = customerTextController.text;
                          requestModel.deviceNumber =
                              deviceNumberController.text;
                          requestModel.timeDelay =
                              int.tryParse(delayTimeController.text);
                          String data = jsonEncode(requestModel.toJson());
                          File file = File(
                              "${Directory.current.path}/request_data.json");
                          file.writeAsStringSync(data);
                          _incrementCounter();
                        }),
                        icon: const Icon(
                          Icons.check_circle,
                          color: Colors.green,
                        ),
                      ),
                      IconButton(
                        iconSize: 40,
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(
                          Icons.cancel_rounded,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
      }),
    );
  }

  showSettingDialog() {
    return showDialog(
      context: context,
      builder: ((context) {
        return StatefulBuilder(builder:
            (BuildContext context, void Function(void Function()) setState) {
          return AlertDialog(
            content: Directionality(
              textDirection: TextDirection.rtl,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SettingDropDownItems(
                    onChanged: (value) {
                      bankIndex = value;
                      setState;
                    },
                  ),
                  textFieldWidget(label: "پورت", controller: portController),
                  textFieldWidget(label: "آدرس Ip", controller: ipController),
                  textFieldWidget(
                      label: "شماره پذیرنده",
                      controller: acceptorNumberController),
                  textFieldWidget(
                      label: "شماره پایانه", controller: payneNumberController),
                  textFieldWidget(
                      label: "شناسه دستگاه",
                      controller: deviceNumberController),
                  textFieldWidget(
                      label: "مدت زمان انتظار",
                      controller: delayTimeController),
                  textFieldWidget(
                      label: "متن روی رسید مشتری",
                      controller: customerTextController),
                  textFieldWidget(
                      label: "مبلغ را وارد کنید",
                      controller: amountTextController),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                        iconSize: 40,
                        onPressed: (() {
                          RequestModel requestModel = RequestModel();
                          requestModel.amount =
                              int.tryParse(amountTextController.text);
                          requestModel.bankName = bankIndex;
                          requestModel.ipAddress = ipController.text;
                          requestModel.portNumber =
                              int.tryParse(portController.text);
                          requestModel.protocol = "LAN";
                          requestModel.payanehNumber =
                              payneNumberController.text;
                          requestModel.acceptorNumber =
                              acceptorNumberController.text;
                          requestModel.comment = customerTextController.text;
                          requestModel.deviceNumber =
                              deviceNumberController.text;
                          requestModel.timeDelay =
                              int.tryParse(delayTimeController.text);
                          String data = jsonEncode(requestModel.toJson());
                          File file = File(
                              "${Directory.current.path}/request_data.json");
                          file.writeAsStringSync(data);
                          _incrementCounter();
                        }),
                        icon: const Icon(
                          Icons.check_circle,
                          color: Colors.green,
                        ),
                      ),
                      IconButton(
                        iconSize: 40,
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        icon: const Icon(
                          Icons.cancel_rounded,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          );
        });
      }),
    );
  }

  textFieldWidget(
      {required final String label,
      required final TextEditingController controller}) {
    return TextField(
      decoration: InputDecoration(
        label: Text(label),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(10),
          borderSide: const BorderSide(color: Colors.black, width: 1),
        ),
      ),
      controller: controller,
    );
  }
}
