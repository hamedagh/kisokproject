import json
import os

import clr

clr.AddReference("dlls/PosBankLib")
from PosBankLib.BLL import ParsianLan
from PosBankLib.BLL import SamanLan
from PosBankLib.BLL import MelliLan
# from PosBankLib.BLL import IranKish
from PosBankLib.BLL import AsanPardakht
from PosBankLib.BLL import MellatLan
from PosBankLib.BLL import BehPardakht

f = open('request_data.json')
data = json.load(f)
parsian = ParsianLan()
saman = SamanLan()
# meli = MelliLan()
# kish = IranKish()
# asan_pardakht = AsanPardakht()
# melat = MellatLan()
# beh_pardakht = BehPardakht()


def sent_to_pos(bank_name, amount, time_delay, comment,  ip_address, terminal_number, port_number,
                acceptor_number):
    res = ()
    match bank_name:
        case 0:
            res = parsian.PayPrice(amount, time_delay, "", ip_address, "", port_number, "")   
            with open('data.json', 'w', encoding='utf-8') as f:
              json.dump({'payment': res[0], 'responseMsg': res[1]}, f, ensure_ascii=False, indent=4) 
            return
        case 1:
            res = saman.PayPrice(amount, time_delay, comment, ip_address, terminal_number, "")
            with open('data.json', 'w', encoding='utf-8') as f:
              json.dump({'payment': res[0], 'responseMsg': res[1]}, f, ensure_ascii=False, indent=4) 
            return        
        case 2:
            res = MelliLan.PayPrice(amount, ip_address, str(port_number) , "")
            with open('data.json', 'w', encoding='utf-8') as f:
              json.dump({'payment': res[0], 'responseMsg': res[1]}, f, ensure_ascii=False, indent=4)     
            return        
        # case 3:
            # res = IranKish.PayPrice(amount, time_delay, comment, ip_address, terminal_number, "")            
        case 4:
            res = AsanPardakht.PayPrice(amount, ip_address, str(port_number), time_delay,"")  
            with open('data.json', 'w', encoding='utf-8') as f:
              json.dump({'payment': res[0], 'responseMsg': res[1]}, f, ensure_ascii=False, indent=4)  
            return         
        case 5:
            res = MellatLan.PayPrice(amount, str(port_number), time_delay, ip_address, "")   
            with open('data.json', 'w', encoding='utf-8') as f:
              json.dump({'payment': res[0], 'responseMsg': res[1]}, f, ensure_ascii=False, indent=4) 
            return         
        case 6:
            res = BehPardakht.PayPrice(amount, ip_address, "")   
            with open('data.json', 'w', encoding='utf-8') as f:
              json.dump({'payment': res[0], 'responseMsg': res[1]}, f, ensure_ascii=False, indent=4) 
            return         





sent_to_pos(data['bankName'],data['amount'], data['timeDelay'], data['comment'], data['ipAddress'], data['payanehNumber'],
            data['portNumber'], data['acceptorNumber'])
